**Docker Terminologies**

1. Docker

Docker is a program for developers to develop, and run applications with containers.


2. Docker Image

A Docker image is contains everything needed to run an applications as a container. This includes:

- code
- runtime
- libraries
- environment variables
- configuration files


The image can then be deployed to any Docker environment and as a container.


3. Container

A Docker container is a running Docker image.
From one image you can create multiple containers .


4. Docker Hub

Docker Hub is like GitHub but for docker images and containers.

**Docker basic commands**

- docker ps  
The docker ps command allows us to view all the containers that are running on the Docker Host.

- docker start  
This command starts any stopped container(s).

- docker stop  
This command stops any running container(s).

- docker run  
This command creates containers from docker images.

- docker rm  
This command deletes the containers.

**Common Operations on Dockers**

- Download/pull the docker images that you want to work with.
- Copy your code inside the docker
- Access docker terminal
- Install and additional required dependencies
- Compile and Run the Code inside docker
- Document steps to run your program in README.md file
- Commit the changes done to the docker.
- Push docker image to the docker-hub and share repository with people who want to try your code.